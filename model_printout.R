# Generate CSV
rm(list = ls())
library(survival)
library(data.table)
library(xtable) #https://cran.r-project.org/web/packages/xtable/vignettes/xtableGallery.pdf
options(xtable.floating = FALSE)
options(xtable.timestamp = "")

smell_considered = c("BVS", "DC", "LS", "UN")
# models
model_list = list()
model_list["BVS"] = "coxph(Surv(tstart, tstop, infect) ~ BVS + MS_dataRep + MS_sync, newdata)"
model_list["DC"] = "coxph(Surv(tstart, tstop, infect) ~ LS + BR_loop + MS_sync + strata(MS_total.cat) + cluster(id), newdata)"
model_list["LS"] = "coxph(Surv(tstart, tstop, infect) ~ LS + BR_parallelism + MS_sync + strata(MS_total.cat) + cluster(id), newdata)"
model_list["UN"] = "coxph(Surv(tstart, tstop, infect) ~ MS_total + exp +strata(UN.cat)+ cluster(id), newdata)"

# Surv_BVS ~ BVS0 + MS_dataRep + MS_sync
# Surv_DC ~ LS + BR_loop + MS_sync + strata(MS_total)
# Surv_LS ~ LS0 + BR_parallelism + MS_sync + strata(MS_total)
# Surv_UN ~ MS_total + exp + strata(UN)


for(smell in smell_considered){
  fname = sprintf("~/Documents/SEAnalyticsAutomation/StatisticalAnalysis/%s_cox_data.csv", smell)
  data <- read.csv(fname, header=TRUE)
  # prepare data
  newdata = tmerge(data1=data[, 1:44], data2=data, id=id, tstop=futime)
  for(eventNum in names(data[45:ncol(data)])){
    lst = as.list(data[eventNum])
    arr = as.numeric(unlist(lst))
    newdata =tmerge(newdata, data, id=id, infect = event(arr))
  }
  newdata = tmerge(newdata, newdata, id, enum=cumtdc(tstart))
  
  newdata$MS_total.cat = cut(newdata$MS_total, c(0,14,17,21))
  newdata$UN.cat = cut(newdata$UN, c(0,4,9,50))
  model = eval(parse(text=model_list[smell]))
  # print(summary(model))
  
# ## construct a summary table
  variables = rownames(coef(summary(model)))
  stat_names = c("coef", "exp(coef)", "se(coef)", "Pr(>|z|)")
  diagnosis = cox.zph(model, transform='rank')
  
  model_table = data.table()
  for(v in variables){
    rowi = list()
    rowi["Variable"] = v
    for(stat in stat_names){
      rowi[stat] = round(coef(summary(model))[v,stat], 3)
    }
    # conf. interval
    interval = exp(confint(model))
    rowi["2.5 %"] = round(interval[v,"2.5 %"],  3)
    rowi["97.5 %"] = round(interval[v,"97.5 %"], 3)
    
    # diagnosis
    rowi["P(PH)"] = round(diagnosis$table[v,"p"], 3)
    
    # append row to table
    model_table = rbind(model_table, rowi)
  }
  
  # generate latex
  model_table.table <- xtable(model_table,digits=3)
  latex_table = print(model_table.table, include.rownames = FALSE, booktabs = TRUE)
  # fname = sprintf("~/home/peeratham/Dropbox/LatexFiles/%s_model.tex", smell)
  fname = sprintf("~/Documents/SEAnalyticsAutomation/StatisticalAnalysis/tables/%s_model.tex", smell)
  fileConn <- file(fname)
  writeLines(latex_table, fileConn)
  close(fileConn)
}







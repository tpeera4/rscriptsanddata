# cox_UN.R
rm(list = ls())
library(survival)
data <- read.csv("~/Documents/SEAnalyticsAutomation/StatisticalAnalysis/UN_cox_data.csv", header=TRUE)
newdata = tmerge(data1=data[, 1:44], data2=data, id=id, tstop=futime)
for(eventNum in names(data[45:ncol(data)])){
  lst = as.list(data[eventNum])
  arr = as.numeric(unlist(lst))
  newdata =tmerge(newdata, data, id=id, infect = event(arr))
}
newdata = tmerge(newdata, newdata, id, enum=cumtdc(tstart))



# newdata$MS_total.cat = cut(newdata$MS_total, c(0,14,17,21))
newdata$sprite.cat = cut(newdata$sprite, c(0,5,10,50))
newdata$UN.cat = cut(newdata$UN, c(0,4,9,50))

m = coxph(Surv(tstart, tstop, infect) ~
          + strata(UN.cat)
          + MS_total
          + exp
          + cluster(id), newdata)

summary(m)
cox.zph(m, transform = 'rank')

# survival curve
UN.history = c(levels(strata(newdata$UN.cat))[3],levels(strata(newdata$UN.cat))[1])
exp_vals = c(1.75, 0.6)
MS_total_vals = c(17, 13)

# effect of experience
exp_pattern = expand.grid( exp=exp_vals,
                          MS_total= mean(data$MS_total, na.rm=TRUE),
                          UN.cat = UN.history)

exp.surv = survfit(m,newdata=exp_pattern)

# effect of overall mastery
mastery_pattern = expand.grid(MS_total=MS_total_vals,
                           exp= mean(data$exp, na.rm=TRUE),
                           UN.cat = UN.history)

mastery.surv = survfit(m,newdata=mastery_pattern)



#Plotting
colours=c("darkgreen", "red") #good->bad
linetypes=c("dotdash","solid","dashed") #bad=>god
draw.new=expand.grid(colour=colours,linetype=linetypes[1:2],stringsAsFactors=F)
cbind(exp_pattern,draw.new)
cbind(mastery_pattern,draw.new)


pdf( "~/Documents/SEAnalyticsAutomation/StatisticalAnalysis/figures/UN_surv.pdf", width = 11, height = 5 )
par(mfrow=c(1,2), oma=c(0, 0, 2, 0))
# experience survival curves
plot(exp.surv,col=draw.new$colour,lty=draw.new$linetype, lwd=1, xmax=20,
     xlab='Projects', ylab=expression(paste("Proportion ", bold("NOT"), " introducing the smell")))
legend("right",legend=exp_vals,title="Experience (years)",fill=colours, lwd=2, cex=0.8, inset = c(.02, 0.2))
legend("topright",legend=UN.history,title="Prior history of UN smell density",lty=linetypes, inset = .02, cex=0.8)

# mastery survival curves
plot(mastery.surv,col=draw.new$colour,lty=draw.new$linetype, lwd=1, xmax=20,
     xlab='Projects', ylab=expression(paste("Proportion ", bold("NOT"), " introducing the smell")))
legend("right",legend=MS_total_vals,title="Overall Mastery",fill=colours, lwd=2, cex=0.8, inset = c(.02, 0.2))
legend("topright",legend=UN.history,title="Prior history of UN smell density",lty=linetypes, inset = .02, cex=0.8)

mtext(expression(bold("UN code smell risk comparison")),outer=TRUE, cex=1.4, line=-2)
dev.off()


